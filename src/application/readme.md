
EtherClue v2.0.0

The following node packages need to be installed to run the solution:  

```
npm install web3
npm install web3-eth-debug
npm install level
npm install command-line-args
npm install command-line-usage
npm install etherscan-api
#npm install node-cache
npm install node-file-cache
npm install big-integer
npm install cli-progress
npm install colors
npm install keccak256
npm install hashmap
npm install selenium-webdriver ?
npm install geckodriver ?
```

Load local modules.

```
.\Update.sh
```

Optional elements that can be added;

- Skip function select logic optimization.
- Run multiple transactions at the same time.